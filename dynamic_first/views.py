from django.shortcuts import render

from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render_to_response

from dynamic_first.forms import Dynamic_Form , Create 
from django.template import RequestContext 
from django.forms.formsets import formset_factory 
import forms 
from django.core.exceptions import ValidationError



# Create your views here.

def choices (request):
    
    
    if request.method == 'POST':
        form = Dynamic_Form (request.POST)
        if form.is_valid () :
          
            name  = form.cleaned_data ['name']
            father_name = form.cleaned_data ['father_name']
         
            mother_name  =  form.cleaned_data ['mother_name']
            phone_no  =  form.cleaned_data ['phone_no']
            mobile_no  =  form.cleaned_data ['mobile_no']
            pin_no  =  form.cleaned_data ['pin_no']
            temporary_address  =  form.cleaned_data ['temporary_address']
            permanent_address =  form.cleaned_data ['permanent_address']
            sex =  form.cleaned_data ['sex']
            age  =  form.cleaned_data ['age']
            caste  =  form.cleaned_data ['caste']
            hobbies   =  form.cleaned_data ['hobbies']
            occupation =  form.cleaned_data ['occupation']
            mother_tounge  =  form.cleaned_data ['mother_tounge']
            political_views  =  form.cleaned_data ['political_views']
            mobile_model  =  form.cleaned_data ['mobile_model']
            
            request.method = 'GET'
            return test (request , name_v = name ,  father_name_v= father_name, mother_name_v  =  mother_name ,  phone_no_v  =  phone_no , mobile_no_v  =  mobile_no,
            pin_no_v  =  pin_no ,
            temporary_address_v =  temporary_address ,
            permanent_address_v =  permanent_address ,sex_v =  sex, 
            age_v  =  age , caste_v  = caste ,
            hobbies_v   = hobbies ,
            occupation_v = occupation ,
            mother_tounge_v  =  mother_tounge,
            political_views_v  = political_views,
            mobile_model_v  = mobile_model )
    
            
    
    else :
        form = Dynamic_Form ()
        return render_to_response('choices.html',{'form': form},context_instance=RequestContext(request,{'y':'subject_1'})) 
   
   
def test (request , name_v =False  ,  father_name_v =False , mother_name_v =False ,  phone_no_v =False , mobile_no_v =False ,
            pin_no_v =False ,
            temporary_address_v =False , 
            permanent_address_v =False ,sex_v=False  , 
            age_v =False ,  caste_v=False  ,
            hobbies_v  =False  , 
            occupation_v  =False ,
            mother_tounge_v =False , political_views_v =False , mobile_model_v  =False ):
    
    Y = ""
    check = [True]*20
    
    if request.method == 'POST':
     
        if request.POST.get('Name',"") != "__notexist" :
            
            name_v = True
            if request.POST.get('Name',"") == '' :
                check[0] = False
                Y += "Please fill the name.\n" 
            
        
         
        if request.POST.get('Father\'s Name',"") != "__notexist" :
             father_name_v = True
             if request.POST.get('Father\'s Name',"") == '' :
                check[1] = False
                Y += "Please fill the father name.\n"
      
        
        if request.POST.get('Mother\'s Name',"") != "__notexist" :
            mother_name_v = True
            if request.POST.get('Mother\'s Name',"") == '' :
                check[0] = False
                Y += "Please fill the mother name.\n"
      
        
        if request.POST.get('Local Phone no.',"") != "__notexist" :
            phone_no_v = True
            if request.POST.get('Local Phone no.',"") == '' :
                check[0] = False
                Y += "Please fill the local phone Number.\n"
        
        
        if request.POST.get('mobile Phone no.',"") != "__notexist" :
            mobile_no_v = True
            if request.POST.get('mobile Phone no.',"") == '' :
                check[0] = False
                Y += "Please fill the mobile phone .\n"
        
        if request.POST.get('Caste',"") != "__notexist" :
            
            caste_v = True
            if request.POST.get('Caste',"") == '' :
                check[0] = False
                Y += "Please fill the Caste .\n"
        
        if request.POST.get('Permanent Address',"") != "__notexist" :
            
            permanent_address_v = True
            if request.POST.get('Permanent Address',"") == '' :
                check[0] = False
                Y += "Please fill the Address .\n"
        
        if request.POST.get('Pin no.',"") != "__notexist" :
            pin_no_v = True
            if request.POST.get('Pin no.',"") == '' :
                check[0] = False
                Y += "Please fill the Pin no .\n"
        
        if request.POST.get('Temporary Address',"") != "__notexist" :
            temporary_address_v = True
            if request.POST.get('Temporary Address',"") == '' :
                check[0] = False
                Y += "Please fill the temporary address .\n"
        
       
         
        if request.POST.get('Your Hobbies',"") != "__notexist" :
            hobbies_v = True
            if request.POST.get('Your Hobbies',"") == '' :
                check[0] = False
                Y += "Please fill your Hobbies\n"
      
        if request.POST.get('Occupation',"") != "__notexist" :
            occupation_v = True
            if request.POST.get('Occupation',"") == '' :
                check[0] = False
                Y += "Please fill the occupation . \n"
        
        if request.POST.get('Mother Tounge',"") != "__notexist" :
            mother_tounge_v = True
            if request.POST.get('Mother Tounge',"") == '' :
                check[0] = False
                Y += "Please fill the mother tounge .\n"
        
        if request.POST.get('Political View',"") != "__notexist" :
            political_views_v = True
            if request.POST.get('Political View',"") == '' :
                check[0] = False
                Y += "Please fill the political view . \n"
        
        
        if request.POST.get('Mobile Model no.',"") != "__notexist" :
            mobile_model_v = True
            if request.POST.get('Mobile Model no.',"") == '' :
                check[0] = False
                Y += "Please fill the model no.\n"
        
        if request.POST.get('Age',"") != "__notexist" :
            age_v = True
            if request.POST.get('Age',"") == '' :
                check[0] = False
                Y += "Please fill the age . \n"
        
        if request.POST.get('Sex',"") != "__notexist" :
            sex_v = True
            if request.POST.get('Sex',"") == '' :
                check[0] = False
                Y += "Please fill the sex .\n"
            
        form = Create (name_s = name_v ,  father_name_s= father_name_v, mother_name_s  =  mother_name_v ,  phone_no_s  =  phone_no_v , mobile_no_s  =  mobile_no_v ,
         pin_no_s  =  pin_no_v ,
        temporary_address_s =  temporary_address_v ,
            permanent_address_s =  permanent_address_v ,sex_s =  sex_v, 
            age_s  =  age_v , caste_s  = caste_v ,
            hobbies_s   = hobbies_v ,
            occupation_s = occupation_v ,
            mother_tounge_s  =  mother_tounge_v ,
            political_views_s  = political_views_v ,
            mobile_model_s  = mobile_model_v  )
      
        count = 0 
        for i in check :
            if i == False :
                count = 1 
                break 
             
        if count == 1 :
            #return HttpResponse("g")
            return render_to_response('qw.html',{'form': form},context_instance=RequestContext(request,{'y':Y})) 
        else :
            return HttpResponse("Your Form has been submitted successfully..... :) :)")
            
         
   
        
             
        
        
        
                 
        
        
    if request.method == 'GET':
        
        
        
        form = Create (name_s = name_v ,  father_name_s= father_name_v, mother_name_s  =  mother_name_v ,  phone_no_s  =  phone_no_v , mobile_no_s  =  mobile_no_v ,
         pin_no_s  =  pin_no_v ,
        temporary_address_s =  temporary_address_v ,
            permanent_address_s =  permanent_address_v ,sex_s =  sex_v, 
            age_s  =  age_v , caste_s  = caste_v ,
            hobbies_s   = hobbies_v ,
            occupation_s = occupation_v ,
            mother_tounge_s  =  mother_tounge_v ,
            political_views_s  = political_views_v ,
            mobile_model_s  = mobile_model_v  )
        return render_to_response('qw.html',{'form': form},context_instance=RequestContext(request,{'y':'Fill the form'})) 
   

        
    
    
    
    
