'''
Created on Jan 17, 2014

@author: Dell
'''

from django import forms  

class Dynamic_Form (forms.Form):
    name = forms.BooleanField (required = False ) ;
    father_name  = forms.BooleanField (required = False) ;
    mother_name  = forms.BooleanField (required = False) ;
    phone_no  = forms.BooleanField (required = False) ;
    mobile_no  = forms.BooleanField (required = False ) ;
    pin_no  = forms.BooleanField (required = False) ;
    temporary_address  = forms.BooleanField (required = False) ;
    permanent_address = forms.BooleanField (required = False) ;
    sex = forms.BooleanField (required = False ) ;
    age  = forms.BooleanField (required = False) ;
    caste  = forms.BooleanField (required = False) ;
    hobbies   = forms.BooleanField (required = False) ;
    occupation = forms.BooleanField (required = False ) ;
    mother_tounge  = forms.BooleanField (required = False) ;
    political_views  = forms.BooleanField (required = False) ;
    mobile_model  = forms.BooleanField (required = False) ;
    
   
   
   
    
class Create (forms.Form):
    def __init__(self,*args,**kwargs):
        
        
        
        '''
        name_s = name_v ,  father_name_s= father_name_v, mother_name_s  =  mother_name_v ,  phone_no_s  =  phone_no_v , mobile_no_s  =  mobile_no_v ,
            pin_no_s  =  pin_no_v ,
            temporary_address_s =  temporary_address_v ,
            permanent_address_s =  permanent_address_v ,sex_s =  sex_v, 
            age_s  =  age_v , caste_s  = caste_v ,
            hobbies_s   = hobbies_v ,
            occupation_s = occupation_v ,
            mother_tounge_s  =  mother_tounge_v ,
            political_views_s  = political_views_v ,
            mobile_model_s  = mobile_model_v
        
        '''
        
        
        
        name_s = kwargs.pop("name_s") ;
        father_name_s = kwargs.pop('father_name_s') ;
        mother_name_s = kwargs.pop("mother_name_s") ;
        phone_no_s = kwargs.pop("phone_no_s") ;
        mobile_no_s = kwargs.pop("mobile_no_s") ;
        caste_s = kwargs.pop("caste_s") ;
        permanent_address_s = kwargs.pop("permanent_address_s") ;
        
        pin_no_s = kwargs.pop("pin_no_s") ;
        temporary_address_s = kwargs.pop('temporary_address_s') ;
        sex_s = kwargs.pop("sex_s") ;
        age_s = kwargs.pop("age_s") ;
        hobbies_s = kwargs.pop("hobbies_s") ;
        
        
        occupation_s = kwargs.pop("occupation_s") ;
        mother_tounge_s = kwargs.pop('mother_tounge_s') ;
        political_views_s = kwargs.pop("political_views_s") ;
        mobile_model_s = kwargs.pop("mobile_model_s") ;
        
        super(Create,self).__init__(*args,**kwargs)
       
        if name_s == True :
          self.fields['Name'] = forms.CharField( required = True )
        else :
           self.fields['Name'] = forms.CharField(label = 'name', widget = forms.HiddenInput,initial = '__notexist')
            
            
        if father_name_s == True :
          self.fields['Father\'s Name'] = forms.CharField( required = True )
        else :
           self.fields['Father\'s Name'] = forms.CharField(label = 'father_name', widget = forms.HiddenInput,initial = '__notexist')
      
      
        if  mother_name_s == True :
          self.fields['Mother\'s Name'] = forms.CharField( required = True )
        else :
           self.fields['Mother\'s Name'] = forms.CharField(label = 'mother_name', widget = forms.HiddenInput,initial = '__notexist')
            
        
        if  phone_no_s == True :
          self.fields['Local Phone no.'] = forms.CharField( required = True )
        else :
           self.fields['Local Phone no.'] = forms.CharField(label = 'phone_no', widget = forms.HiddenInput,initial = '__notexist')
           
           
        if  mobile_no_s == True :
          self.fields['Mobile Phone no.'] = forms.CharField( required = True )
        else :
           self.fields['mobile Phone no.'] = forms.CharField(label = 'mobile_no', widget = forms.HiddenInput,initial = '__notexist')
            
        if  caste_s == True :
          self.fields['Caste'] = forms.CharField(required = True )
        else :
           self.fields['Caste'] = forms.CharField(label = 'caste', widget = forms.HiddenInput,initial = '__notexist')
            
            
        if  permanent_address_s == True :
          self.fields['Permanent Address'] = forms.CharField( required = True )
        else :
           self.fields['Permanent Address'] = forms.CharField(label = 'permanent_address', widget = forms.HiddenInput,initial = '__notexist')
          
          
        if  pin_no_s == True :
          self.fields['Pin no.'] = forms.CharField( required = True )
        else :
           self.fields['Pin no.'] = forms.CharField(label = 'pin_no', widget = forms.HiddenInput,initial = '__notexist')
            
            
            
        if  temporary_address_s == True :
          self.fields['Temporary Address'] = forms.CharField( required = True )
        else :
           self.fields['Temporary Address'] = forms.CharField(label = 'temporary_address', widget = forms.HiddenInput,initial = '__notexist')
            
            
        if  pin_no_s == True :
          self.fields['Pin no.'] = forms.CharField( required = True )
        else :
           self.fields['Pin no.'] = forms.CharField(label = 'pin_no', widget = forms.HiddenInput,initial = '__notexist')
            
            
        if   hobbies_s == True :
          self.fields['Your Hobbbies'] = forms.CharField(required = True )
        else :
           self.fields['Your Hobbies'] = forms.CharField(label = 'hobbies', widget = forms.HiddenInput,initial = '__notexist')
           
           
        if  occupation_s == True :
          self.fields['Occupation'] = forms.CharField( required = True )
        else :
           self.fields['Occupation'] = forms.CharField(label = 'occupation', widget = forms.HiddenInput,initial = '__notexist')
           
        
        if   mother_tounge_s == True :
          self.fields['Mother Tounge'] = forms.CharField( required = True )
        else :
           self.fields['Mother Tounge'] = forms.CharField(label = 'mother_tounge', widget = forms.HiddenInput,initial = '__notexist')
           
           
        if   political_views_s  == True :
          self.fields['Political View'] = forms.CharField( required = True )
        else :
           self.fields['Political View'] = forms.CharField(label = 'political_views', widget = forms.HiddenInput,initial = '__notexist')
            
      
          
        if mobile_model_s  == True :
          self.fields['Mobile Model no.'] = forms.CharField( required = True )
        else :
           self.fields['Mobile Model no.'] = forms.CharField(label = 'mobile_model', widget = forms.HiddenInput,initial = '__notexist')
           
        if mobile_model_s  == True :
          self.fields['Mobile Model no.'] = forms.CharField(  required = True )
        else :
           self.fields['Mobile Model no.'] = forms.CharField(label = 'mobile_model', widget = forms.HiddenInput,initial = '__notexist')
            
        if age_s  == True :
          self.fields['Age'] = forms.CharField( required = True , widget = forms.DateInput )
        else :
           self.fields['Age'] = forms.CharField( widget = forms.HiddenInput ,initial = '__notexist')
           
           
        if sex_s  == True :
          self.fields['Sex'] = forms.CharField(required = True )
        else :
           self.fields['Sex'] = forms.CharField(label = 'sex', widget = forms.HiddenInput ,initial = '__notexist')
           
           
    def clean(self):
        cleaned_data = self.cleaned_data
        password = ""
        
        
        if password == "" :
            raise forms.ValidationError(u"Passwords do not match.")
        return cleaned_data
            
       
        
       
       
    
    
    
     
   
    


